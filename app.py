from flask import Flask, request, jsonify
from flask_restful import Resource, Api
import pickle
import pandas as pd
import json
from sklearn.preprocessing import StandardScaler
import io, csv
from flask_csv import send_csv
app = Flask(__name__)
api = Api(app)

model = pickle.load(open("pickle_model.pkl", "rb"))
stdModel = pickle.load(open("st.pkl", "rb"))
# 'month_1','month_2','month_3','BottomHoleLatitude','BottomHoleLongitude','LonWGS84','LatWGS84'


class Home(Resource):
    def get(self):

        return {"well": "/well", "array": "/array", "csv": "/csv"}


class Well(Resource):
    def get(self):
        return {
            "information": "month_1 indicates the production of the peak month and month_2 and month_3 indicate the following months",
            "format": {
                "month_1": "int",
                "month_2": "int",
                "month_3": "int",
                "BottomHoleLatitude": "int",
                "BottomHoleLongitude": "int",
                "LonWGS84": "int",
                "LatWGS84": "int",
            },
        }

    def post(self):
        reqjson = request.get_json()
        reqDf = pd.DataFrame(reqjson, index=[0])
        reqDf[
            [
                "month_1",
                "month_2",
                "month_3",
                "BottomHoleLatitude",
                "BottomHoleLongitude",
                "LonWGS84",
                "LatWGS84",
            ]
        ] = stdModel.transform(
            reqDf[
                [
                    "month_1",
                    "month_2",
                    "month_3",
                    "BottomHoleLatitude",
                    "BottomHoleLongitude",
                    "LonWGS84",
                    "LatWGS84",
                ]
            ]
        )
        return {"3_year_cum": model.predict(reqDf)[0]}


class Array(Resource):
    def get(self):
        return {
            "information": "enter an array with as many items as desired month_1 indicates the production of the peak month and month_2 and month_3 indicate the following months",
            "format": [
                {
                    "api": "string",
                    "month_1": "int",
                    "month_2": "int",
                    "month_3": "int",
                    "BottomHoleLatitude": "int",
                    "BottomHoleLongitude": "int",
                    "LonWGS84": "int",
                    "LatWGS84": "int",
                }
            ],
        }

    def post(self):
        reqjson = request.get_json()
        reqDf = pd.DataFrame(
            reqjson,
            columns=[
                "api",
                "month_1",
                "month_2",
                "month_3",
                "BottomHoleLatitude",
                "BottomHoleLongitude",
                "LonWGS84",
                "LatWGS84",
            ],
        )
        reqDf[
            [
                "month_1",
                "month_2",
                "month_3",
                "BottomHoleLatitude",
                "BottomHoleLongitude",
                "LonWGS84",
                "LatWGS84",
            ]
        ] = stdModel.transform(
            reqDf[
                [
                    "month_1",
                    "month_2",
                    "month_3",
                    "BottomHoleLatitude",
                    "BottomHoleLongitude",
                    "LonWGS84",
                    "LatWGS84",
                ]
            ]
        )
        reqDf["3_year_cum"] = model.predict(reqDf.drop("api", axis=1))
        # print(reqDf[['api','3_year_cum']].to_json(orient='records'))
        return json.loads(reqDf[["api", "3_year_cum"]].to_json(orient="records"))


class Csv(Resource):
    def get(self):
            return {
            "information": "you can use test.csv as guide, input an csv with as many items as desired month_1 indicates the production of the peak month and month_2 and month_3 indicate the following months",
            "format": 'api,month_1,month_2,month_3,BottomHoleLatitude,BottomHoleLongitude,LonWGS84,LatWGS84',
        }    
    def post(self):
        reqjson = request.get_json()
        reqDf = pd.read_csv(request.files[""],dtype={'api':str})
        reqDf[
            [
                "month_1",
                "month_2",
                "month_3",
                "BottomHoleLatitude",
                "BottomHoleLongitude",
                "LonWGS84",
                "LatWGS84",
            ]
        ] = stdModel.transform(
            reqDf[
                [
                    "month_1",
                    "month_2",
                    "month_3",
                    "BottomHoleLatitude",
                    "BottomHoleLongitude",
                    "LonWGS84",
                    "LatWGS84",
                ]
            ]
        )
        reqDf["pred"] = model.predict(reqDf.drop("api", axis=1))
        output =[]
        for index,row in reqDf.iterrows():            
            output.append({'api':row.api,'3_year_cum':str(row.pred)})
        return send_csv(output,'predictions.csv',['api','3_year_cum'])


api.add_resource(Home, "/")
api.add_resource(Well, "/well")
api.add_resource(Array, "/array")
api.add_resource(Csv, "/csv")
if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0',port=8080)
